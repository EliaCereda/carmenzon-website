jQuery(function ($) {

	function show_loading(totale, callback) {
		$("#loading").modal({
			backdrop: 'static',
			keyboard: false
		});
		$("#loading .bar").width("0");

		/*	Quando la barra è piena, attendi la fine dell'animazione ed esegui
			il callback */
		$("#loading .bar").on($.support.transition.end, function() {
			if (current == totale) {

                //Eseguil il callback se esiste, altrimenti non fare nulla
	            (callback || $.noop)();
			}
		})

		var current = 0;
		return function () {
			current++;

	   		//Imposta la barra di caricamento alla percentuale corrente
	   		var width = (current / totale) * 100;
	   		$("#loading .bar").width(width + "%");
	   }
	}

	/*	Mostra la schermata di caricamento, la funzione window.progress aumenta la
		barra progresso */
	window.progress = show_loading(3, function () {
		$("#loading").modal("hide")
		$("#container").show();
	});

	/****************/
	/*   PRODOTTI   */
	/****************/

	window.carica_prodotti = function (batch, callback) {
		var request =  gapi.client.carmenzon.prodotti.list()
		function oncomplete(risultato) {
			risultato = risultato.result || risultato;

			$("#prodotti").data("prodotti", {})

			$(risultato.items).each(function (indice) {
				$("#prodotti").data("prodotti")[this.entityKey] = this
			})

			var tabella = $("#prodotti table tbody"),
				template = $("#prodotti table .template");
			
			//Rimuovi le righe aggiunte precedentemente che non siano il template
			tabella.find("tr:not(.template)").remove();
			$('#prodotti .popover-container .popover').popover('hide');

			$(risultato.items).each(function (indice) {
				//La funzione each imposta la variabile `this` al prodotto corrente

				var riga = template.clone().data("prodotto", this)

				riga.removeClass("template hide")

				riga.find(".immagine").attr("src", (this.url_immagine) ? this.url_immagine + "=s100-c" : "holder.js/100x100")
				riga.find(".nome").text(this.nome)
				riga.find(".descrizione").text(this.descrizione)
				riga.find(".prezzo").text(this.prezzo.toFixed(2))

				var badge;
				switch(this.rimasti) {
					case null:
					case undefined:
					case 0:
						badge = ""
						break;

					case 1:
					case 2:
						badge = "badge-important"
						break;
					
					case 3:
					case 4:
						badge = "badge-warning"
						break;

					default:
						badge = "badge-success"
				}
				riga.find(".rimasti").append(
					$("<span />", {
						"class": "badge " + badge,
						text: this.rimasti || 0
					})
				)

				riga.find(".btn-elimina").popover({
					title: "<strong>Elimina</strong>",
					content: "Sei sicuro di voler eliminare questo prodotto?				\
						<br /><br />														\
						<button class=\"btn btn-danger btn-elimina-conferma\">Conferma</button>		\
						<h3 class=\"icon-spinner icon-spin icon-large pull-right hide\"></h3>",
					html: "true",
					placement: "top",
					container: "#prodotti .popover-container"
				}).click(function () {
					//Quando viene cliccato il bottone Elimina associa il prodotto al popover

					var $this = $(this),
						prodotto = $this.parents("tr").data("prodotto"),
						popover = $this.data("popover")

					popover.$tip.data("prodotto", prodotto)
					popover.$tip.data("popover", popover)
				})

				riga.appendTo(tabella);
			})

			Holder.run();

			(callback || $.noop)();
		}

		if (batch) {
			batch.add(request, { callback: oncomplete })
		} else {
			request.execute(oncomplete);
		}
	}
	
	$("#crea_prodotto").click(function () {
		$("#modal_modifica_prodotto").data("prodotto", null).modal();
	});
	
	$('#prodotti table tbody').on("click", ".btn-modifica", function() {
        var prodotto = $(this).parents("tr").data("prodotto");
		
		$("#modal_modifica_prodotto").data("prodotto", prodotto).modal();
    });
	
	$("#modal_modifica_prodotto").on("show", function () {
		var $this = $(this);

		if ($this.data("prodotto") == null) {
			$this.find(".modal-header h3").text("Crea prodotto");
		} else {
			$this.find(".modal-header h3").text("Modifica prodotto");
		}

		var prodotto = $.extend({
					nome: "",
					descrizione: "",
					prezzo: 0.0,
					rimasti: null,
					url_immagine: ""
				}, $this.data("prodotto")
			);

		$("#nomeProdotto").attr("value", prodotto.nome);
		$("#descrizioneProdotto").attr("value", prodotto.descrizione);
		$("#prezzoProdotto").attr("value", (prodotto.prezzo) ? prodotto.prezzo.toFixed(2) : "");
		$("#rimastiProdotto").attr("value", (prodotto.rimasti != null) ? prodotto.rimasti : "");
		$("#thumbnailProdotto").attr("src", (prodotto.url_immagine) ? prodotto.url_immagine + "=s30" : "holder.js/30x30");
		
		Holder.run();
		$this.data("prodotto", prodotto);
	});
	$("#immagineProdotto").on("change", function () {
		var file = this.files[0],
			reader = new FileReader();

		$(reader).on("load", function () {
			$("#thumbnailProdotto").attr("src", this.result);
		})
		reader.readAsDataURL(file);
	});
	$("#modal_modifica_prodotto").on("submit", function (e) {
		//Non inviare il form con il metodo standard del browser
		e.preventDefault();

		var $this = $("#modal_modifica_prodotto"),
			prodotto = $.extend($this.data("prodotto"), {
				nome: $("#nomeProdotto").val(),
				descrizione: $("#descrizioneProdotto").val(),
				prezzo: parseFloat($("#prezzoProdotto").val()),
				rimasti: parseInt($("#rimastiProdotto").val())
			});

		$this.find(".icon-spinner").show()
		$this.find(":input").attr("disabled", true)

		gapi.client.carmenzon.prodotti.put(prodotto).execute(function (risultato) {
			function completa() {
				$("#modal_modifica_prodotto").modal("hide");

				$this.find(".icon-spinner").hide()
				$this.find(":input").attr("disabled", false)
			}

			var files = $("#immagineProdotto").get(0).files;

			if (files.length != 0) {
				var dataURI = $("#thumbnailProdotto").attr("src").split(",")

				//Togli l'inizio dell'URL
				dataURI.shift()

				var content = dataURI.join(",")

				gapi.client.carmenzon.prodotti.upload({
					entityKey: risultato.entityKey,
					immagine: content
				}).execute(function () {
					completa();
				});
			} else {
				completa();
			}
		});
	});

	$('#prodotti .popover-container').on("click", ".btn-elimina-conferma", function() {
        var $this = $(this),
        	popover = $this.parents(".popover"),
        	prodotto = popover.data("prodotto");
		
		popover.find(".icon-spinner").show()
		popover.find(":input").attr("disabled", true)

		gapi.client.carmenzon.prodotti.delete({ entityKey: prodotto.entityKey }).execute($.noop)
    });

    /****************/
	/*    ORDINI    */
	/****************/

	$("#ordini .container").masonry({
		itemSelector: ".item"
	})

	$('a[data-toggle="tab"]').on('shown', function () {
		$("#ordini .container").masonry('reload')
	})

	window.carica_ordini = function (batch, callback) {
		var request1 = gapi.client.carmenzon.ordini.list()
		function oncomplete1(risultato) {
			risultato = risultato.result || risultato;

			$("#ordini").data("ordini", risultato.items)

			var container = $("#ordini .container"),
				template = $("#ordini .template .item"),
				utenti = $("#ordini").data("utenti");
			
			//Rimuovi gli ordini aggiunte precedentemente
			container.masonry('remove', container.find(".item"));

			$(risultato.items).each(function (indice) {
				//La funzione each imposta la variabile `this` al prodotto corrente

				var item = template.clone().data("ordine", this),
					tabella = item.find("table"),
					template_riga = tabella.find(".template");

				var utente = utenti[this.proprietario] || {};
				item.find(".nome").text(utente.nome || this.proprietario);
				item.find(".classe").text(utente.classe || "");

				$(this.prodotti).each(function () {
					var riga = template_riga.clone()

					riga.removeClass("template hide")

					var prodotto = $("#prodotti").data("prodotti")[this.prodotto] || {}

					riga.find(".immagine").attr("src", (prodotto.url_immagine) ? prodotto.url_immagine + "=s50-c" : "holder.js/50x50")
					riga.find(".prodotto").text(prodotto.nome)
					riga.find(".quantita").text((this.quantita) ? "x" + this.quantita : "--")

					riga.appendTo(tabella);
				})

				if (this.pronto) {
					item.find(".pronto").addClass("btn-success").attr("disabled", true);
					item.find(".modal-overlay").show();

                    //Forza il ridisegno della pagina
					item.find(".modal-overlay").offset().left;

					item.find(".modal-overlay").addClass("in");
				}

				item.find(".pronto").click(function () {
					$(this).addClass("btn-success").attr("disabled", true)
					item.find(".icon-spinner").show()

					var ordine = $(this).parents(".item").data("ordine")

					ordine.pronto = true

					gapi.client.carmenzon.ordini.put(ordine).execute(function () {
						item.find(".icon-spinner").hide()

						item.find(".modal-overlay").show();

                    	//Forza il ridisegno della pagina
						item.find(".modal-overlay").offset().left;

						item.find(".modal-overlay").addClass("in");
					})
				})

				item.appendTo(container)
			})
			
			Holder.run();

			container.masonry('reload');

			(callback || $.noop)();
		}

		var request2 = gapi.client.carmenzon.utenti.list()
		function oncomplete2(risultato) {
			risultato = risultato.result || risultato;

			$("#ordini").data("utenti", {})

			$(risultato.items).each(function (){
				$("#ordini").data("utenti")[this.user_id] = this
			})
		} 

		if (batch) {
			batch.add(request2, { callback: oncomplete2 })
			batch.add(request1, { callback: oncomplete1 })
		} else {
			request2.execute(function (risultato) {
				oncomplete2(risultato);

				request1.execute(oncomplete1);
			})
		}
	}

	window.attiva_notifiche = function () {
		gapi.client.carmenzon.channeltoken.get().execute(function (risultato) {
			var channel = new goog.appengine.Channel(risultato.token),
				socket = channel.open({
					onopen: function () {
						console.log("opened")
					},
					onmessage: function (message) {
						data = JSON.parse(message.data)

						console.log(data)

						if (data.ricarica == "prodotti") {
							window.carica_prodotti()
						} else if (data.ricarica == "ordini" || data.ricarica == "utenti") {
							window.carica_ordini()
						}
					},
					onerror: function (err) {
						console.log(err)
					},
					onclose: function () {
						console.log("closed, reopening");

						window.attiva_notifiche();
					}
				})
		})
	}
});

function endpoints_init() {
	//Aspetta che jQuery sia pronto prima di continuare
	jQuery(function ($) {
		endpoints_login(true, endpoints_authed);
	});
}

function endpoints_authed(login_info) {
	//Se il login non riesce automaticamente, mostra la finestra di login.
	if (login_info) {
		window.progress();

		gapi.client.load('carmenzon', 'v1', function () {
			window.progress();
			
			var httpBatch = gapi.client.newRpcBatch();

			window.carica_prodotti(httpBatch);
			window.carica_ordini(httpBatch);

			httpBatch.execute(function () {
				window.progress();

				window.attiva_notifiche();
			})
		}, 'https://carmenzon-api.appspot.com/_ah/api');
	} else {
		endpoints_login(false, endpoints_authed);
	}
}
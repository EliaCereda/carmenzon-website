jQuery(function ($) {
    //Abilita le animazioni delle immagini
    $('#myCarousel').carousel();

    //Associa il login con il bottone Accedi (solo quando è abilitato)
    $('.navbar-form').on("click", "#accedi:enabled", function() {
        endpoints_login(false, endpoints_authed)
    });
});

function endpoints_init() {
    jQuery(function ($) {
        $('#accedi').removeAttr('disabled')
    });

    //Tenta di eseguire il login in anticipo: se l'utente ha già usato Carmenzon in passato
    //quando premerà il bottone Accedi il login sarà immediato
    endpoints_login(true, function (login_info) {
        if (login_info != null) {
            gapi.client.request({
                path: "/oauth2/v2/userinfo",
                callback: function(response){
                    jQuery(function ($) {
                        $('#nome-utente').show();

                    //Forza il ridisegno della pagina
                    $('#nome-utente').offset().left;

                    $('#nome-utente').addClass("in").find("span").text(response.name)
                })
                }
            })
        }
    });
}

function endpoints_authed(login_info) {
    window.location = "admin.html";
}
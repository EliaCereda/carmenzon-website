jQuery(function ($) {

    //Abilita lo scroll animato per tutti i link interni alla pagina
    $('a').smoothScroll();

});

function endpoints_login(immediate, callback) {
    if (endpoints_login.login_info) {
        jQuery(function ($) {
            if ($.isFunction(callback)) {
                callback(endpoints_login.login_info);
            }
        });
    } else {
        gapi.auth.authorize({
            client_id: "1088793215507-calebl9e6ufdfgk3r8a7sft9hhh92ovi.apps.googleusercontent.com",
            scope: [
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile'
            ],
            immediate: immediate,
            response_type: 'token id_token'
        }, function (_login_info) {
            endpoints_login.login_info = _login_info;

            jQuery(function ($) {
                (callback || $.noop)(_login_info);
            });
        });
    }
}